/*
 * JBoss, Home of Professional Open Source
 * Copyright 2011 Red Hat Inc. and/or its affiliates and other
 * contributors as indicated by the @author tags. All rights reserved.
 * See the copyright.txt in the distribution for a full listing of
 * individual contributors.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package org.infinispan.quickstart.clusteredcache;

import com.hazelcast.config.Config;
import com.hazelcast.config.InMemoryFormat;
import com.hazelcast.config.MapConfig;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.examples.AllTest;
import org.infinispan.configuration.cache.CacheMode;
import org.infinispan.configuration.cache.ConfigurationBuilder;
import org.infinispan.configuration.global.GlobalConfigurationBuilder;
import org.infinispan.manager.DefaultCacheManager;


import java.io.IOException;
import java.util.Map;
import java.util.Random;

public class Node {

   private final String nodeName;
   private final String putValue;
   private final int count;


   public Node(String nodeName, int count, int size) {
      this.nodeName = nodeName;
      this.count = count;
      this.putValue = randomString(size);
   }

   public static String randomString(int length) {
      String str = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
      Random random = new Random();
      StringBuffer buf = new StringBuffer();
      for (int i = 0; i < length; i++) {
         int num = random.nextInt(62);
         buf.append(str.charAt(num));
      }
      return buf.toString();
   }

   public static void main(String[] args) throws Exception {
      String nodeName = null;
      int size = 100;
      int count = 0;

      for (String arg : args) {

         if (arg.startsWith("-c")) {
            count = Integer.parseInt(arg.substring(2));
         } else if (arg.startsWith("-s")) {
            size = Integer.parseInt(arg.substring(2));
         } else {
            nodeName = arg;
         }
      }
      new Node(nodeName, count, size).test();
   }

   public void test() throws IOException, InterruptedException {
      run(createHazelcastMap());
//      run(createInfinispanCache());
   }

   public void run(final Map<String, String> cache) throws IOException, InterruptedException {
      System.console().readLine();

      Thread putThread = new Thread() {
         @Override
         public void run() {
            cache.put("init cache" , putValue);

            long start = System.currentTimeMillis();
            int counter = count;
            while (counter-- != 0) {
               cache.put("key-" + counter, putValue);
            }
            long end = System.currentTimeMillis();
            System.out.println("Elapsed milliseconds: " + (end - start));
         }
      };
      putThread.start();
      putThread.join();
      printCacheSize(cache);
   }

   private void printCacheSize(Map<String, String> cache) {
      System.out.println("size: " + cache.size());
   }

   private org.infinispan.Cache<String, String> createInfinispanCache() {
      System.out.println("Starting a cache manager with a programmatic configuration");
      DefaultCacheManager cacheManager = new DefaultCacheManager(
            GlobalConfigurationBuilder.defaultClusteredBuilder()
                  .transport().nodeName(nodeName).addProperty("configurationFile", "jgroups.xml")
                  .build(),

            new ConfigurationBuilder()
                  .clustering()
                  .cacheMode(CacheMode.DIST_SYNC)
                  .hash().numOwners(2)
                  .build()
      );
      org.infinispan.Cache<String, String> cache = cacheManager.getCache();
      // Add a listener so that we can see the puts to this node
      // cache.addListener(new LoggingListener());
      return cache;
   }

   private Map<String, String> createHazelcastMap() {
      Config cfg = new Config();
      cfg.getMapConfig("default").setInMemoryFormat(InMemoryFormat.OBJECT).setBackupCount(1);
      HazelcastInstance hz = Hazelcast.newHazelcastInstance(cfg);
      Map<String, String> map = hz.getMap("cache-test");
      return map;
   }
}