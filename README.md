Clustered Cache Quickstart
==========================

This quickstart demonstrates *embedded cache* running on *two node* in *Java SE*.

* Compile the application by running `mvn clean compile dependency:copy-dependencies -DstripVersion`

* To try with a *distributed* cache, run the following command in separated terminals:
    * `java -cp target/classes:target/dependency/* org.infinispan.quickstart.clusteredcache.distribution.Node0`
    * `java -cp target/classes:target/dependency/* org.infinispan.quickstart.clusteredcache.distribution.Node1`
    * `java -cp target/classes:target/dependency/* org.infinispan.quickstart.clusteredcache.distribution.Node2`
